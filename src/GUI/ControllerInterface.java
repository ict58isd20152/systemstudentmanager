/*
 * Created by Hien Nguyen with Netbeans IDE
 * Date: 17-5-2016
 */
package GUI;

import Entity.monHoc;
import Entity.sinhVien;

/**
 *
 * @author hiennq
 */
public interface ControllerInterface {
    public void themSinhVien(String maSV,String ho,String ten,String diaChi,String lop);
    public void xoaSinhVien(String maSV);
    public void themMonHoc(String maMon,String tenMonHoc,int soDonVi);
    public void themBangDiem(int diem,sinhVien sinhVien,monHoc monHoc);
    public sinhVien getSinhVien(String ms);
}
