/*
 * Created by Hien Nguyen with Netbeans IDE
 * Date: 17-5-2016
 */
package GUI;

import DataAccess.FileBD;
import DataAccess.FileMH;
import DataAccess.FileSV;
import Entity.bangDiem;
import Entity.monHoc;
import Entity.sinhVien;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hiennq
 */
public abstract class Controller extends javax.swing.JFrame implements ControllerInterface{
    /** Creates new form GuiManager */
    ArrayList<sinhVien> listSV = new ArrayList<>();
    ArrayList<monHoc> listMH = new ArrayList<>();
    ArrayList<bangDiem> listBD = new ArrayList<>();
    
    public Controller(){
        try {
                listSV=(ArrayList<sinhVien>)new FileSV("sv.dat").load();
                listMH=(ArrayList<monHoc>)new FileMH("mh.dat").load();
                listBD=(ArrayList<bangDiem>)new FileBD("bd.dat").load();
            } 
        catch (FileNotFoundException ex) {
                Logger.getLogger(GuiManager.class.getName()).log(Level.SEVERE, null, ex);
            } 
        catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(GuiManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    
    @Override
    public void themSinhVien(String maSV,String ho,String ten,String diaChi,String lop){
        sinhVien modelSinhVien = new sinhVien(maSV,lop,ho,ten,diaChi);
        listSV.add(modelSinhVien);
        new FileSV("sv.dat").save(listSV);
    }
    
    @Override
    public void xoaSinhVien(String maSV){

    }
    
    @Override
    public void themMonHoc(String maMon,String tenMonHoc,int soDonVi){
        monHoc modelMonHoc = new monHoc(maMon,tenMonHoc, soDonVi);
        listMH.add(modelMonHoc);
        new FileMH("mh.dat").save(listMH);
    }
    
    @Override
    public void themBangDiem(int diem,sinhVien sinhVien,monHoc monHoc){
        bangDiem a = new bangDiem(diem,sinhVien,monHoc);
        System.out.println(""+a.getMh().getTenMon());
        listBD.add(a);
        new FileBD("bd.dat").save(listBD);
    }
    
    /**
     *
     * @param ms
     * @return
     */
    @Override
    public sinhVien getSinhVien(String ms){
        sinhVien a=null;
        for(sinhVien i :listSV)
        {
            if(i.getMaSV().equals(ms))
            {
                return i;
            }
        }
        return a;
    }
    
    public void getSinhVien(){
        System.out.println("List Sinh Vien: ");
        for(sinhVien i :listSV)
        {
            System.out.println(i.getMaSV()+" "+i.getHo()+" "+i.getTen()+" "+i.getDiaChi()+" "+i.getLop());
        }
    }
 
    public void getMonHoc(){
        System.out.println("List Mon Hoc: ");
        for(monHoc i :listMH)
        {
            System.out.println(i.getMaMon()+" "+i.getTenMon()+" "+i.getSoDonviht());
        }
    }
    
    public monHoc getMonHoc(String maMH){
        monHoc monhoc = null;
        for(monHoc i: listMH)
        {
            if(i.getMaMon().equalsIgnoreCase(maMH))
                monhoc = i;
        }
        return monhoc;
    }
    
    public boolean ktraTrungMonHoc(String a)
    {
        return getMonHoc(a)==null;
    }
    
    public boolean ktraTrungMaSv(String a)
    {
        return getSinhVien(a)==null;
    }

}
