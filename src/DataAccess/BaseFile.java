/*
 * Created by Hien Nguyen with Netbeans IDE
 * Date: 17-5-2016
 */
package DataAccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author hiennq
 */
public class BaseFile {
    private String fileName;
    protected File f;
    
    public BaseFile() {
        
    }
    
    public BaseFile(String fileName) {
        setFileName(fileName);
        setFile();
    }
    
    public void setFileName(String name){
        fileName=name;
    }
    
    public void setFile(){
        f = new File(fileName);
    }
    
    public void save(ArrayList<?> list){
        try{
            if(!f.exists())
                f.createNewFile();
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))) {
                oos.writeObject(list);
            }
        }   
        catch(Exception e){}
    }
    
    public ArrayList<?> load() throws FileNotFoundException, IOException, ClassNotFoundException{
        if(!f.exists())
            return new ArrayList<>();
        ObjectInputStream ois =new  ObjectInputStream(new FileInputStream(f));
        return (ArrayList<?>)ois.readObject();
    }
   
}
